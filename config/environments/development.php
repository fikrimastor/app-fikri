<?php
/**
 * Configuration overrides for WP_ENV === 'development'
 */

use Roots\WPConfig\Config;

Config::define('SAVEQUERIES', true);
Config::define('WP_DEBUG', true);
Config::define('WP_DEBUG_DISPLAY', true);
Config::define('SCRIPT_DEBUG', true);

ini_set('display_errors', 1);

// Enable plugin and theme updates and installation from the admin
Config::define('DISALLOW_FILE_MODS', false);

/** Swift Performance Timeout */
Config::define('SWIFT_PERFORMANCE_PREBUILD_TIMEOUT', env('SWIFT_PERFORMANCE_PREBUILD_TIMEOUT') ?: 300);

// define('WP_CACHE', env('WP_CACHE') ?: true); // Added by W3 Total Cache

// /**
//  * Wordpress Social Login Setup
//  */

// if (!defined('wsl_settings_Facebook_app_id')){
//     Config::define('WSL_SETTINGS_FACEBOOK_APP_ID', env('FACEBOOK_APP_ID'));
//     // Config::define('wsl_settings_Google_app_id', env('GOOGLE_APP_ID'));
// }

// if (!defined('wsl_settings_Facebook_app_secret')){
//     Config::define('wsl_settings_Facebook_app_secret', env('FACEBOOK_APP_SECRET'));
//     // Config::define('wsl_settings_Google_app_secret', env('GOOGLE_APP_SECRET'));
// }


// if ('wsl_settings_' . $provider . '_app_id'){
//     define('wsl_settings_Facebook_app_id', env('FACEBOOK_APP_ID'));
//     // Config::define('WSL_SETTINGS_FACEBOOK_APP_ID', env('FACEBOOK_APP_ID'));
//     // Config::define('WSL_SETTINGS_FACEBOOK_APP_SECRET', env('FACEBOOK_APP_ID'));
//     define('wsl_settings_Google_app_id', env('GOOGLE_APP_ID'));
// }

// if ('wsl_settings_' . $provider . '_app_secret' ){
//     define('wsl_settings_Google_app_secret', env('GOOGLE_APP_SECRET'));
//     define('wsl_settings_Facebook_app_secret', env('FACEBOOK_APP_SECRET'));
// }

